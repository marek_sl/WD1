# coding=utf-8
capitals = {"France": "Paris",
            "Iceland": "Reykjavik",
            "Denmark": "Copenhagen",
            "Litauen": "Vilnius",
            "Canada": "Ottawa",
            "Österreich": "Wien"}

print "Study the following list:"
print
print capitals.items()
print

# items gives a list of tuples
for country_capital in capitals.items():
    print country_capital

# we can unpack in forloop
for country, capital in capitals.items():
    print country, capital


print capitals.keys()
print capitals.values()
