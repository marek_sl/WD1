a_dict = {}
points = {"alfred": 10,
          "bettina": 100,
          "christian": 50,
          "doris": 75}

print points  # {'christian': 50, 'bettina': 100, 'doris': 75, 'alfred': 10}

# reference
print points["alfred"]

for name in ["alfred", "bettina"]:
    print name, points[name]

# change
points["alfred"] = 30
print points
points["franz"] = 40
print points
