print True
print False


print (True and False)  # -> False
print (True or False)   # -> True


small_number = 2
big_number = 100

print small_number > big_number     # False
print small_number < big_number     # True
print small_number == big_number     # False
print small_number != big_number     # True
print small_number is big_number     # Fslse
print id(small_number), id(big_number)
print small_number is not big_number     # False

print "small_number > big_number: ", small_number > big_number  # -> False


# 0000 0001   = 1
# 0000 0011   = 3

print 1 & 3  # = 1
print 1 | 3  # = 3

# 0000 0001   = 1
# 0000 0011   = 3

# 0000 0001   = 1&3
# 0000 0011   = 1|3

print 1 | 2  # = 3
# 0000 0001   = 1
# 0000 0010   = 2
# 0000 0011   = 3
