from datetime import datetime, tzinfo, timedelta
import dateutil.tz
import pytz
import dateutil.parser


class simple_utc(tzinfo):
    def tzname(self,**kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)


print datetime.utcnow().replace(tzinfo=simple_utc()).isoformat()

print datetime(2013, 9, 11, 0, 17, tzinfo=dateutil.tz.tzoffset(None, 9*60*60)).isoformat()


dates = [
'20130910001700+02',
'20131010001700+02',
'20131110001700+02',
]


for date in dates:
    dt = dateutil.parser.parse(date)  # read and convert to timezone aware datetime
    print dt.strftime("%Y%m%d%H%M%S%z")[:-2],
    print dt.isoformat().replace("T",""), type(dt), dt.tzinfo,
    dt_utc = dt.astimezone(pytz.utc)  # convert to utc
    print dt_utc.isoformat(),

    amsterdam = pytz.timezone('CET')

    dt_cet = dt.astimezone(amsterdam)
    print dt_cet.isoformat()






