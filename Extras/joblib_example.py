import multiprocessing
import os
import urllib
import bs4
import re
import joblib

def follow_link_and_download(link):
    book_id = re.findall(r"\d+\Z", link)
    if not book_id:
        return
    else:
        book_id = book_id[0]
    filename = "{}.txt".format(book_id)
    if os.path.exists(filename):
        print("Already Downloaded")
        return
    suburl = "http://www.gutenberg.org/files/{0}/".format(book_id)
    subcontent = urllib.request.urlopen(suburl).read()
    subsoup = bs4.BeautifulSoup(subcontent, "html5lib")
    file_link = subsoup.find_all("a", text=re.compile("{}.*\.txt".format(book_id, )))
    if not file_link:
        return
    else:
        file_link = file_link[0]
    book_subcontent = urllib.request.urlopen(
        "http://www.gutenberg.org/" + str(book_id) + "/" + file_link["href"]).read()
    with open(filename, "wb") as f:
        f.write(book_subcontent)
    return

def main():
    url = "http://www.gutenberg.org/browse/languages/de"
    content = urllib.request.urlopen(url).read()
    soup = bs4.BeautifulSoup(content, "html5lib")
    entry_div = soup.body.find("div", attrs={"class":"pgdbbylanguage"})
    links = entry_div.findAll("a", attrs={"href":True})
    href_links = [i.get("href") for i in links if "ebook" not in i.get("href")]

    # With parallel processing
    # num_cores = multiprocessing.cpu_count()
    # joblib.Parallel(n_jobs=num_cores, verbose=50)(joblib.delayed(follow_link_and_download)(i) for i in href_links)

    # without parallelization
    for link in links:
        follow_link_and_download(link)

if __name__ == '__main__':
    main()
