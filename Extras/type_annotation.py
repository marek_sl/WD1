import random


def lottery():
    return random.sample(range(1, 50), 6)


def string_compare(content):
    # type: (str) -> bool
    return content.isdigit()


if __name__ == '__main__':
    print lottery()
    print string_compare("1")
    print string_compare("a")
