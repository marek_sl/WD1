import numpy as np
if __name__ == '__main__':
    x = np.array([[1, 2], [3, 4]])

    print(np.sum(x))  # Compute sum of all elements; prints "10"
    print(np.sum(x, axis=0))  # Compute sum of each column; prints "[4 6]"
    print(np.sum(x, axis=1))  # Compute sum of each row; prints "[3 7]"

    x = np.array([[1, 2], [3, 4]])
    print(x)  # Prints "[[1 2]
    #          [3 4]]"
    print(x.T)  # Prints "[[1 3]
    #          [2 4]]"

    # Note that taking the transpose of a rank 1 array does nothing:
    v = np.array([1, 2, 3])
    print(v)  # Prints "[1 2 3]"
    print(v.T)  # Prints "[1 2 3]"
