# Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText


def safety_question():
    while True:
        answer = raw_input('\n\n#################\nDo you really want to send a message to following Emails:\n######\n%s\n######\nConfirm with [y]es, or [n]o:'%'\n'.join([recipient]))
        if 'y' in answer.lower():
            print('Sending...')
            break
        if 'n' in answer.lower():
            print('Exiting')
            exit()

if __name__ == '__main__':
    # sender
    print("="*20)
    gmail_user = "GMAIL_ACCOUNT"
    print("=" * 20)
    gmail_password = "PASSWORD"
    print("=" * 20)

    # email server settings
    try:
        print('Logging in to GMail...')
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
    except Exception as err:
        print('Something went wrong...')
        print(err)
        exit()

    print("=" * 20)
    email_subject = raw_input('Enter Email subject:\n')
    print("=" * 20)

    # recipient
    recipient = raw_input('Recipient Email:\n') #  put your email here
    print("=" * 20)

    safety_question()

    # make message
    textfile = "Hey '%s', what\'s up?\n\n{} Message sent by EmailBot!".format(gmail_user)
    msg = MIMEText(textfile)
    msg['Subject'] = email_subject
    msg['From'] = gmail_user
    msg['To'] = recipient

    # send email:
    server.sendmail(gmail_user, [recipient], msg.as_string())
    print('Sent to {}'.format(recipient))

    # finish
    server.quit()
    print('All sent')

