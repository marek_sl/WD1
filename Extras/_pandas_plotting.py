import pandas
import random
import matplotlib.pyplot as plt

index = range(1000)
data = [x*random.random() for x in index]

df = pandas.DataFrame(data,index)

print df.mean()
print df.std()

df.plot()
plt.show()
