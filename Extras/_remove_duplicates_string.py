# coding=utf-8
import string

text = "Hello this is a valid text"

if __name__ == '__main__':
    l_text = list(text)
    seen = []
    def remover(x, seen):
        print x
        if x not in seen:
            seen.append(x)
        else:
            l_text.remove(x)

    [remover(x, seen) for x in l_text]
    print "".join(seen)
