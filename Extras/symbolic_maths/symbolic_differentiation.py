import math
import sympy as sym


x = sym.Symbol('x')
y = sym.Symbol('y')
t = sym.Symbol('t')
alpha = sym.Symbol('alpha')


conc = 20*(math.e**(-alpha*t)-math.e**(-1.2*t))
print("-"*20)
print("Initial function: c(t,alpha) = ")
print(conc.evalf() )
print("-"*20)
print("c(10,0.25) = ")
print(conc.subs({t: 10, alpha: 0.25}) )
print("-"*20)
print("c(10,0.25) = ")
print(conc.subs([(t, 10), (alpha, 0.25)]) )

conc_dt1 = sym.diff(conc, t, 1)
conc_dt2 = sym.diff(conc, t, 2)
print("-"*20)
print("c'(t,alpha) = ")
print(conc_dt1)
print("-"*20)
print("c''(t,alpha) = ")
print(conc_dt2)
print("-"*20)
print("c'(10,0.25) = ")
print(conc_dt1.subs({t: 10, alpha: 0.25}) )
print("-"*20)
print("simplified c''(t,alpha) = ")
print(sym.simplify(conc_dt2) )
