
if __name__ == '__main__':
    words = "hi,world,great".split(",")
    print(words)
    # CRLF vs CR vs LF
    joined_words = "\n".join(words)
    print(joined_words)
