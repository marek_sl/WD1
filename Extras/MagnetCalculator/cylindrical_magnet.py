import math

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

remanence_field = 0.5  # in Tesla, product of permeability (4 pi * 10^-7) and remanence magnetization
magnetic_radius = 0.03  # in meter
magnetic_height = 0.01  # along z axis


def calc_B_field(remanence, radius, height, distance):
    """calculate the magnetic flux density B at distance from a cylindrical magnet

    :param remanence:
    :param radius:
    :param height:
    :param distance:
    :return:
    """

    B = remanence / 2. * ((distance + height) / (math.sqrt((distance + height) ** 2 + radius ** 2)) - distance / (
    math.sqrt(distance ** 2 + radius ** 2)))
    return B


if __name__ == '__main__':
    distances = np.linspace(0.3, 0.5, 100)
    df = pd.DataFrame(index=distances)
    all_values = []
    df[str(magnetic_height) + "-" + str(magnetic_radius)] = np.array(
        [calc_B_field(remanence_field, magnetic_radius, magnetic_height, z) * 1e6 for z in distances])
    df.plot(title="Radius:{}, Height: {}".format(magnetic_radius, magnetic_height), colormap='jet', marker="o")
    plt.xlabel("distance $[m]$")
    plt.ylabel("Magnetic Flux $[\mu T]$")
    plt.show()
