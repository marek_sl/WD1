"""Needs Python 3 and fbchat package, install it with pip"""
import fbchat


class MessageDownloader(fbchat.Client):
    def __init__(self, username, password):
        super().__init__(username, password)
        self.contacts = self.get_contacts()

    def get_contacts(self):
        friends = self.fetchAllUsers()
        contacts = {}
        for friend in friends:
            contacts[friend.name] = friend.uid
        return contacts


if __name__ == '__main__':
    username = "ENTER_NAME"
    password = "ENTER_PASSWORD"
    downloader = MessageDownloader(username, password)
    print("Found following contacts: ")
    all_contacts = downloader.get_contacts()
    for c in sorted(all_contacts):
        print(c)
