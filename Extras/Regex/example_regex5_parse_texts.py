# coding=utf-8
import re
import collections

text = """Dinosaurs are a diverse group of reptiles[note 1] of the clade Dinosauria that first appeared during the 
Triassic period. Although the exact origin and timing of the evolution of dinosaurs is the subject of active 
research,[1] the current scientific consensus places their origin between 231 and 243 million years ago.[2] They 
became the dominant terrestrial vertebrates after the Triassic–Jurassic extinction event 201 million years ago. Their 
dominance continued through the Jurassic and Cretaceous periods and ended when the Cretaceous–Paleogene extinction 
event led to the extinction of most dinosaur groups 66 million years ago. The fossil record indicates that birds are 
modern feathered dinosaurs,[3] having evolved from theropod ancestors during the Jurassic Period.[4] As such, 
birds were the only dinosaur lineage to survive the mass extinction event.[5] Throughout the remainder of this 
article, the term "dinosaur" is sometimes used generically to refer to the combined group of avian dinosaurs (birds) 
and non-avian dinosaurs; at other times it is used to refer to the non-avian dinosaurs specifically, while the avian 
dinosaurs are sometimes simply referred to as "birds". This article deals primarily with non-avian dinosaurs. 
Dinosaurs are a varied group of animals from taxonomic, morphological and ecological standpoints. Birds, at over 10,
000 living species,[6] are the most diverse group of vertebrates besides perciform fish.[7] Using fossil evidence, 
paleontologists have identified over 500 distinct genera[8] and more than 1,000 different species of non-avian 
dinosaurs.[9] Dinosaurs are represented on every continent by both extant species (birds) and fossil remains.[10] 
Through the first half of the 20th century, before birds were recognized to be dinosaurs, most of the scientific 
community believed dinosaurs to have been sluggish and cold-blooded. Most research conducted since the 1970s, 
however, has indicated that all dinosaurs were active animals with elevated metabolisms and numerous adaptations for 
social interaction. Some are herbivorous, others carnivorous. Evidence suggests that egg laying and nest building are 
additional traits shared by all dinosaurs. While dinosaurs were ancestrally bipedal, many extinct groups included 
quadrupedal species, and some were able to shift between these stances. Elaborate display structures such as horns or 
crests are common to all dinosaur groups, and some extinct groups developed skeletal modifications such as bony armor 
and spines. While the dinosaurs' modern-day surviving avian lineage (birds) are generally small due to the 
constraints of flight, many prehistoric dinosaurs (non-avian and avian) were large-bodied—the largest sauropod 
dinosaurs are estimated to have reached lengths of 39.7 meters (130 feet)[11] and heights of 18 meters (59 feet)[12] 
and were the largest land animals of all time. Still, the idea that non-avian dinosaurs were uniformly gigantic is a 
misconception based in part on preservation bias, as large, sturdy bones are more likely to last until they are 
fossilized. Many dinosaurs were quite small: Xixianykus, for example, was only about 50 cm (20 in) long. Since the 
first dinosaur fossils were recognized in the early 19th century, mounted fossil dinosaur skeletons have been major 
attractions at museums around the world, and dinosaurs have become an enduring part of world culture. The large sizes 
of some dinosaur groups, as well as their seemingly monstrous and fantastic nature, have ensured dinosaurs' regular 
appearance in best-selling books and films, such as Jurassic Park. Persistent public enthusiasm for the animals has 
resulted in significant funding for dinosaur science, and new discoveries are regularly covered by the media. """

# find all adverbs
print re.findall("\w+ly", text)
print

# find all words with capital letter in the beginning
print re.findall("[A-Z]\w+", text)
print

# count occurrence of all words
counter = collections.Counter(re.findall("\w+", text))
print counter.most_common(20)
