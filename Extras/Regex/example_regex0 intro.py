"""
Python offers two different primitive operations based on regular expressions:
re.match() checks for a match only at the beginning of the string
re.search() checks for a match anywhere in the string.
"""
import re

re.match("c", "abcdef")    # No match
re.search("c", "abcdef")   # Match
re.search("a", "abcdef")   # Match


"""
'^' can be used with search()
"""

re.match("c", "abcdef")    # No match
re.search("^c", "abcdef")  # No match
re.search("^a", "abcdef")  # Match

"""
But: MULTILINE mode match() only matches at the beginning of the string,
whereas using search() with a regular expression beginning with '^'
"""

re.match('X', 'A\nB\nX', re.MULTILINE)  # No match
re.search('^X', 'A\nB\nX', re.MULTILINE)  # Match.