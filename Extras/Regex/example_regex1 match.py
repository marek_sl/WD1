"""
match() returns Matchobjects.

Matchobjects have the method group([group1, ...])
Returns one or more subgroups of the match.
If there is a single argument, the result is a single string;
if there are multiple arguments, the result is a tuple with one item per argument.
Without arguments, group1 defaults to zero (the whole match is returned).
If a groupN argument is zero, the corresponding return value is the entire matching string;
if it is in the inclusive range [1..99], it is the string matching the corresponding parenthesized group.
If a group number is negative or larger than the number of groups defined in the pattern,
an IndexError exception is raised.
If a group is contained in a part of the pattern that did not match, the corresponding result is None.
If a group is contained in a part of the pattern that matched multiple times, the last match is returned.
"""
import re

m = re.match(r"(\w+) (\w+)", "Isaac Newton, physicist") # match 2 words seperated by ' '
print m.group(0)       # The entire match
print m.group(1)       # The first parenthesized subgroup.
print m.group(2)       # The second parenthesized subgroup.
print m.group(1, 2)    # Multiple arguments give us a tuple

print "Entire match indices: ", m.start(0), m.end(0)
print "First group indices: " , m.start(1), m.end(1)
print "Second group indices: ", m.start(2), m.end(2)


"""
If the regular expression uses the (?P<name>...) syntax, the groupN arguments may also be strings identifying groups by their group name.
If a string argument is not used as a group name in the pattern, an IndexError exception is raised.
"""

m = re.match(r"(?P<first_name>\w+) (?P<last_name>\w+)", "Malcolm Reynolds")
print m.group('first_name')
print m.group('last_name')

"""
We can look for 2 words seperated by 'are'
https://www.tutorialspoint.com/python/python_reg_expressions.htm
"""

line = "Cats are smarter than dogs"

matchObj = re.match( r'(.*) are (.*?) .*', line, re.M|re.I)

if matchObj:
   print "matchObj.group() : ", matchObj.group()
   print "matchObj.group(1) : ", matchObj.group(1)
   print "matchObj.group(2) : ", matchObj.group(2)
else:
   print "No match!!"