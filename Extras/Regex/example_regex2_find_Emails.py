# coding=utf-8
import re

text = 'John Hillnob, 52, male, Oxford, john1@2.hillnob@ninjamail.org'

print text

print re.findall('[\w.]+@[\w.]+', text)

"""
we are looking for a combination of alphanumeric (\w) of . characters before and after an @ symbol.
spaces or special characters are not allowed

[]
these brackets show, that we are looking for a set including all alphanumerics and . characters
ex. [abc], this set would check for any of the characters a,b,c

+
plus makes regex look for any number of repetitions of the pattern or character described before
"""


