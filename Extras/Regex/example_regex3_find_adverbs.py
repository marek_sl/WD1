# coding=utf-8
import re

text = "He was carefully disguised but captured quickly by police."


# find words with ly at the end
print re.findall(r"\w+ly", text)

# find words of 5 letters with ly at the end
print re.findall(r"\w{5}ly", text)

# find words of 5 letters with ly at the end, beginning with ' ' character
print re.findall(r" \w{5}ly", text)

# find words of 5 letters with ly at the end, in front of which there is a ' '
print re.findall(r" (\w{5}ly)", text)


"""
Examples for special characters:

'.'
(Dot.) In the default mode, this matches any character except a newline.
If the DOTALL flag has been specified, this matches any character including a newline.

'*'
Causes the resulting RE to match 0 or more repetitions of the preceding RE, as many repetitions as are possible.
ab* will match ‘a’, ‘ab’, or ‘a’ followed by any number of ‘b’s.

'+'
Causes the resulting RE to match 1 or more repetitions of the preceding RE.
ab+ will match ‘a’ followed by any non-zero number of ‘b’s; it will not match just ‘a’.

'?'
Causes the resulting RE to match 0 or 1 repetitions of the preceding RE.
ab? will match either ‘a’ or ‘ab’.

*?, +?, ??
The '*', '+', and '?' qualifiers are all greedy; they match as much text as possible.
Sometimes this behaviour isn’t desired;
if the RE <.*> is matched against <a> b <c>, it will match the entire string, and not just <a>.
Adding ? after the qualifier makes it perform the match in non-greedy or minimal fashion;
as few characters as possible will be matched. Using the RE <.*?> will match only <a>.

{m}
Specifies that exactly m copies of the previous RE should be matched; fewer matches cause the entire RE not to match.
For example, a{6} will match exactly six 'a' characters, but not five.

{m,n}
Causes the resulting RE to match from m to n repetitions of the preceding RE,
attempting to match as many repetitions as possible.
For example, a{3,5} will match from 3 to 5 'a' characters.
Omitting m specifies a lower bound of zero, and omitting n specifies an infinite upper bound.
As an example, a{4,}b will match aaaab or a thousand 'a' characters followed by a b, but not aaab.
The comma may not be omitted or the modifier would be confused with the previously described form.


[]
Used to indicate a set of characters. In a set:

Characters can be listed individually, e.g. [amk] will match 'a', 'm', or 'k'.
Ranges of characters can be indicated by giving two characters and separating them by a '-',
for example [a-z] will match any lowercase ASCII letter, [0-5][0-9] will match all the two-digits numbers from 00 to 59,
 and [0-9A-Fa-f] will match any hexadecimal digit.
 If - is escaped (e.g. [a\-z]) or if it’s placed as the first or last character (e.g. [a-]), it will match a literal '-'.
Special characters lose their special meaning inside sets.
For example, [(+*)] will match any of the literal characters '(', '+', '*', or ')'.
Character classes such as \w or \S (defined below) are also accepted inside a set,
although the characters they match depends on whether LOCALE or UNICODE mode is in force.
Characters that are not within a range can be matched by complementing the set.
If the first character of the set is '^', all the characters that are not in the set will be matched.
For example, [^5] will match any character except '5', and [^^] will match any character except '^'.
^ has no special meaning if it’s not the first character in the set.
To match a literal ']' inside a set, precede it with a backslash,
or place it at the beginning of the set. For example, both [()[\]{}] and []()[{}] will both match a parenthesis.


(...)
Matches whatever regular expression is inside the parentheses, and indicates the start and end of a group;
the contents of a group can be retrieved after a match has been performed,
and can be matched later in the string with the \number special sequence, described below.
To match the literals '(' or ')', use \( or \), or enclose them inside a character class: [(] [)].


\d
When the UNICODE flag is not specified, matches any decimal digit; this is equivalent to the set [0-9].
UNICODE, it will match whatever is classified as a decimal digit in the Unicode character properties database.

\D
When the UNICODE flag is not specified, matches any non-digit character; this is equivalent to the set [^0-9].
With UNICODE, it will match anything other than character marked as digits in the Unicode character properties database.

\s
When the UNICODE flag is not specified, it matches any whitespace character,
this is equivalent to the set [ \t\n\r\f\v].
The LOCALE flag has no extra effect on matching of the space.
If UNICODE is set, this will match the characters [ \t\n\r\f\v]
plus whatever is classified as space in the Unicode character properties database.

\S
When the UNICODE flag is not specified, matches any non-whitespace character;
this is equivalent to the set [^ \t\n\r\f\v] The LOCALE flag has no extra effect on non-whitespace match.
If UNICODE is set, then any character not marked as space in the Unicode character properties database is matched.

\w
When the LOCALE and UNICODE flags are not specified, matches any alphanumeric character and the underscore;
this is equivalent to the set [a-zA-Z0-9_].
With LOCALE, it will match the set [0-9_] plus whatever characters are defined as alphanumeric for the current locale.
If UNICODE is set, this will match the characters [0-9_] plus whatever is classified as alphanumeric in the Unicode
character properties database.

\W
When the LOCALE and UNICODE flags are not specified, matches any non-alphanumeric character;
this is equivalent to the set [^a-zA-Z0-9_]. With LOCALE, it will match any character not in the set [0-9_],
and not defined as alphanumeric for the current locale. If UNICODE is set, this will match anything other than [0-9_]
plus characters classified as not alphanumeric in the Unicode character properties database.
"""