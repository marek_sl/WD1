import re

if __name__ == '__main__':
    searchtext1 = "12 ounces"
    searchtext2 = "1.5 ounces chopped"
    searchtext3 = "Section 1.5  -1.443234   +19999.2323  -123123.123e12  -3e4.5"

    print "Extract a consecutive digits"
    print
    print re.findall('(\d+)', searchtext1)
    print re.findall('(\d+)', searchtext2)
    print re.findall('(\d+)', searchtext3)
    print
    print "Extract a consecutive digits with possible . in middle"
    print
    print re.findall('(\d+\.?\d+)', searchtext1)
    print re.findall('(\d+\.?\d+)', searchtext2)
    print re.findall('(\d+\.?\d+)', searchtext3)
    print
    print "Extract a Decimal Number"
    print
    print re.findall('(\d+(?:\.\d*)?|\.\d+)', searchtext1)
    print re.findall('(\d+(?:\.\d*)?|\.\d+)', searchtext2)
    print re.findall('(\d+(?:\.\d*)?|\.\d+)', searchtext3)
    print
    print "Extract a Decimal Number followed by whitespace"
    print
    print re.findall('(\d+(?:\.\d*)?|\.\d+)\s+', searchtext1)
    print re.findall('(\d+(?:\.\d*)?|\.\d+)\s+', searchtext2)
    print re.findall('(\d+(?:\.\d*)?|\.\d+)\s+', searchtext3)
    print
    print "Extract a Decimal Number followed by whitespace which can be negative"
    print
    print re.findall('([-]?\d+\.\d+)', searchtext1)
    print re.findall('([-]?\d+\.\d+)',  searchtext2)
    print re.findall('([-]?\d+\.\d+)',  searchtext3)
    print
    print
    print "Extract a Decimal Number followed by whitespace and also extract non-whitespace expression after"
    print
    print re.findall('(\d+(?:\.\d*)?|\.\d+)\s+(\S+)', searchtext1)
    print re.findall('(\d+(?:\.\d*)?|\.\d+)\s+(\S+)', searchtext2)
    print re.findall('(\d+(?:\.\d*)?|\.\d+)\s+(\S+)', searchtext3)