import os

# dunder variables (double underscore)
# __file__

if __name__ == '__main__':
    print __file__
    print os.path.dirname(__file__)
    print os.path.join(os.path.dirname(__file__), "example2.py")
    print os.listdir(os.path.dirname(__file__))
    print os.path.exists(os.path.join(os.path.dirname(__file__), "example2.py"))
    print os.path.exists(__file__)
