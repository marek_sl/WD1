"""
for this to run you need to install amqpstorm with
pip install amqpstorm
(https://github.com/eandersson/amqpstorm)

you also need to have a running rabbit mq server
requirements are erlang: http://www.erlang.org/downloads
and rabbit mq: https://www.rabbitmq.com/

This script has been tested for RabbitMQ 3.6.12 and OTP 20.1 Windows 64-bit

start the server: go to sbin/ folder
install plugins for management:
rabbitmq-plugins enable rabbitmq_shovel rabbitmq_management amqp_client
rabbitmq-server start

please run the rabbit mq server on:
HOST: 127.0.0.1
PORT: 5672
"""

import logging
import threading
import itertools
import amqpstorm
import time

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger()


class Publisher(threading.Thread):
    def run(self):
        self.counter = itertools.count()
        with amqpstorm.Connection('127.0.0.1', 'guest', 'guest') as connection:
            with connection.channel() as channel:
                # Declare the Queue, 'simple_queue'.
                channel.queue.declare('simple_queue')

                # Message Properties.
                properties = {
                    'content_type': 'text/plain',
                    'headers': {'key': 'value'}
                }

                # Create the message.
                while 1:
                    message = amqpstorm.Message.create(channel, 'Hello World! {}'.format(self.counter.next()), properties)

                    # Publish the message to a queue called, 'simple_queue'.
                    message.publish('simple_queue')


class Consumer(object):
    def __init__(self, max_retries=None):
        self.max_retries = max_retries
        self.connection = None

    def create_connection(self):
        """Create a connection.
        :return:
        """
        attempts = 0
        while True:
            attempts += 1
            try:
                self.connection = amqpstorm.Connection('127.0.0.1', 'guest', 'guest')
                break
            except amqpstorm.AMQPError as why:
                LOGGER.exception(why)
                if self.max_retries and attempts > self.max_retries:
                    break
                time.sleep(min(attempts * 2, 30))
            except KeyboardInterrupt:
                break

    def start(self):
        """Start the Consumers.
        :return:
        """
        if not self.connection:
            self.create_connection()
        while True:
            try:
                channel = self.connection.channel()
                channel.queue.declare('simple_queue')
                channel.basic.consume(self, 'simple_queue', no_ack=False)
                channel.start_consuming(to_tuple=False)
                if not channel.consumer_tags:
                    channel.close()
            except amqpstorm.AMQPError as why:
                LOGGER.exception(why)
                self.create_connection()
            except KeyboardInterrupt:
                self.connection.close()
                break

    def __call__(self, message):
        print("Message:", message.body)
        message.ack()

if __name__ == '__main__':
    publisher = Publisher()
    publisher.start()
    CONSUMER = Consumer()
    CONSUMER.start()
