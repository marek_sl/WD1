"""
for this to run you need to install amqpstorm with
pip install amqpstorm
(https://github.com/eandersson/amqpstorm)

you also need to have a running rabbit mq server
requirements are erlang: http://www.erlang.org/downloads
and rabbit mq: https://www.rabbitmq.com/

This script has been tested for RabbitMQ 3.6.12 and OTP 20.1 Windows 64-bit

start the server: go to sbin/ folder
install plugins for management:
rabbitmq-plugins enable rabbitmq_shovel rabbitmq_management amqp_client
rabbitmq-server start

please run the rabbit mq server on:
HOST: 127.0.0.1
PORT: 5672
"""

import logging
import threading
import itertools
from amqpstorm import Connection
from amqpstorm import Message

logging.basicConfig(level=logging.DEBUG)

class Publisher(threading.Thread):
    def run(self):
        self.counter = itertools.count()
        with Connection('127.0.0.1', 'guest', 'guest') as connection:
            with connection.channel() as channel:
                # Declare the Queue, 'simple_queue'.
                channel.queue.declare('simple_queue')

                # Message Properties.
                properties = {
                    'content_type': 'text/plain',
                    'headers': {'key': 'value'}
                }

                # Create the message.
                while 1:
                    message = Message.create(channel, 'Hello World! {}'.format(self.counter.next()), properties)

                    # Publish the message to a queue called, 'simple_queue'.
                    message.publish('simple_queue')


def on_message(message):
    """This function is called on message received.
    :param message:
    :return:
    """
    print("Message:", message.body)

    # Acknowledge that we handled the message without any issues.
    message.ack()

    # Reject the message.
    # message.reject()

    # Reject the message, and put it back in the queue.
    # message.reject(requeue=True)


def consumer():
    with Connection('127.0.0.1', 'guest', 'guest') as connection:
        with connection.channel() as channel:
            # Declare the Queue, 'simple_queue'.
            channel.queue.declare('simple_queue')

            # Set QoS to 100.
            # This will limit the consumer to only prefetch a 100 messages.

            # This is a recommended setting, as it prevents the
            # consumer from keeping all of the messages in a queue to itself.
            channel.basic.qos(100)

            # Start consuming the queue 'simple_queue' using the callback
            # 'on_message' and last require the message to be acknowledged.
            channel.basic.consume(on_message, 'simple_queue', no_ack=False)

            try:
                # Start consuming messages.
                # to_tuple equal to False means that messages consumed
                # are returned as a Message object, rather than a tuple.
                channel.start_consuming(to_tuple=False)
            except KeyboardInterrupt:
                channel.close()

if __name__ == '__main__':
    publisher = Publisher()
    publisher.start()
    consumer()
