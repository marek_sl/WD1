import itertools
import random
import threading
import time
import Queue


class Worker(threading.Thread):
    def __init__(self, queue):
        self.__queue = queue
        threading.Thread.__init__(self)

    def run(self):
        while 1:
            event = self.__queue.get()
            if event is None:
                break
            print("WORKER:", event)


class TaskProducer(threading.Thread):
    def __init__(self, queue):
        super(TaskProducer, self).__init__()
        self.events_queue = queue
        self.counter = itertools.count()

    def run(self):
        while 1:
            self.produce_tasks()

    def produce_tasks(self):
        event = (self.counter.next(), random.randint(0, 100))
        print("PRODUCER:", event)
        self.events_queue.put(event)
        time.sleep(1)


if __name__ == '__main__':
    queue = Queue.LifoQueue()
    task_producer = TaskProducer(queue)
    worker = Worker(queue)
    task_producer.start()
    worker.start()
