#!/usr/bin/env python
import os
import random

import jinja2
import webapp2

from models import Message, Person

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        return self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        return self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        return self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        result = self.render_template("index.html", params={})
        return result


class InfoHandler(BaseHandler):
    def get(self):
        result = self.render_template("infosite.html")
        return result


class ManagerHandler(BaseHandler):
    def get(self):
        persons = Person.query(Person.deleted==False).fetch()
        result = self.render_template("manager.html", params={"persons_list": persons})
        return result

    def post(self):
        pressed_button = self.request.get("button", None)
        if pressed_button == "create_mock_entries":
            create_contacts()
        elif pressed_button == "delete":
            account_emails_to_delete = self.request.POST.getall('delete')
            persons = Person.query(Person.email.IN(account_emails_to_delete)).fetch()
            for person in persons:
                # person.key.delete()
                person.deleted = True
                person.put()
        elif pressed_button == "add":
            new_first_name = self.request.get("new_first_name")
            new_last_name = self.request.get("new_last_name")
            email = self.request.get("new_email")
            person = Person(first_name=new_first_name, last_name=new_last_name, email=email)
            person.put()

        persons = Person.query(Person.deleted==False).fetch()
        result = self.render_template("manager.html", params={"persons_list": persons})
        return result


class BlogHandler(BaseHandler):
    def get(self):
        message_list = Message.query(Message.deleted == False).fetch()
        return self.render_template("blog.html", params={"message_list": message_list})

    def post(self):
        new_message_text = self.request.get("text")
        new_message_author = self.request.get("author")
        new_message = Message(message_text=new_message_text, author=new_message_author)
        new_message.put()

        message_list = Message.query(Message.deleted == False).fetch()
        return self.render_template("blog.html", params={"message_list": message_list})


class EditMessageHandler(BaseHandler):
    def get(self, message_id):
        message = Message.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("message_edit.html", params=params)

    def post(self, message_id):
        new_text = self.request.get("some_text")
        author = self.request.get("author")
        message = Message.get_by_id(int(message_id))
        message.message_text = new_text
        message.author = author
        message.put()
        return self.redirect_to("blog")


class DeleteMessageHandler(BaseHandler):
    def get(self, message_id):
        message = Message.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("message_delete.html", params=params)

    def post(self, message_id):
        message = Message.get_by_id(int(message_id))
        # message.key.delete()
        message.deleted = True
        message.put()
        return self.redirect_to("blog")


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
    webapp2.Route("/info", InfoHandler),
    webapp2.Route("/manager", ManagerHandler),
    webapp2.Route("/blog", BlogHandler, name="blog"),
    webapp2.Route('/message/<message_id:\d+>/edit', EditMessageHandler),
    webapp2.Route('/message/<message_id:\d+>/delete', DeleteMessageHandler),
], debug=True)


def main():
    from paste import httpserver
    httpserver.serve(app, host='0.0.0.0', port='8080')


def create_contacts():
    abc_ascii_codes = range(97, 123)
    ABC_ascii_codes = range(65, 91)
    possible_abc_ascii_codes = abc_ascii_codes + ABC_ascii_codes
    name_length = 10
    for x in range(5):
        first_name = "".join([chr(random.choice(possible_abc_ascii_codes)) for _ in range(name_length)])
        last_name = "".join([chr(random.choice(possible_abc_ascii_codes)) for _ in range(name_length)])
        email = "".join([chr(random.choice(possible_abc_ascii_codes)) for _ in range(name_length)]) + "@example.com"
        person = Person(first_name=first_name, last_name=last_name, email=email)
        person.put()


if __name__ == '__main__':
    main()
