#!/usr/bin/env python
import os
import jinja2
import webapp2
import datetime

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        return self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        return self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        return self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        name = "Obama"
        return self.render_template("main.html", params={"name": name})


class AboutUs(BaseHandler):
    def get(self):
        return self.render_template("about_us.html")


class DatetimeHandler(BaseHandler):
    def get(self):
        current_time = datetime.datetime.now()
        current_time_text = current_time.isoformat()
        return self.render_template("time.html", params={"now_time": current_time_text})


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
    webapp2.Route('/about_us', AboutUs),
    webapp2.Route('/current_time', DatetimeHandler),
], debug=True)
