#!/usr/bin/env python
import os
import jinja2
import webapp2
import random

from Persons import Contact

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        return self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        return self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        return self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        result = self.render_template("index.html", params={})
        return result


class InfoHandler(BaseHandler):
    def get(self):
        result = self.render_template("infosite.html")
        return result


class ManagerHandler(BaseHandler):
    def get(self):
        result = self.render_template("manager.html",
                                      params={"persons_list": PERSONS})
        return result

    def post(self):
        result = self.render_template("manager.html",
                                      params={"persons_list": PERSONS})
        return result

app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
    webapp2.Route("/info", InfoHandler),
    webapp2.Route("/manager", ManagerHandler),
], debug=True)


def main():
    from paste import httpserver
    httpserver.serve(app, host='0.0.0.0', port='8080')


def create_contacts():
    persons_list = []

    for x in range(5):
        first_name = str(random.randint(1000000,9000000))
        last_name = str(random.randint(1000000,9000000))
        email = str(random.randint(1000000,9000000)) + "@example.com"
        person = Contact(first_name,last_name,email)

        persons_list.append(person)

    return persons_list

if __name__ == '__main__':

    PERSONS = create_contacts()

    main()
