
Bewegen:
ENDE
POS 1
STRG  Pfeil

Markieren:
STRG + SHIFT + Pfeil

Undo und Redo:
CTRL + Z
CTRL + SHIFT + Z

Jump in Code:
ALT + Click

Terminal Tipps

cd <path>                       change directory to <path>
cd ..                           go up
cd ~                            go to home
cd -                            go to previous
ls                              list
pwd                             path working directory
history                         verlauf der eingegebenen befehle
history | grep <suchbegriff>    verlauf durchsuchen nach <suchbegriff>
which <executable>              look for path to <executable>



Git Tips:

Allgemein:
Mögliche Zustände sind:
------------------ LOKAL -------------------   ---REMOTE---
Untracked -> Tracked -> Stages -> Committed -> Pushed
         add         add       commit       push

Staged -> Tracked/Untracked
      reset

Staged -> komplett weg
      reset --hard

Staged/Tracked -> weg
               checkout -- <filename>
               checkout -- .

Klonen:
git clone <repo-addresse>

Tracken und stagen:
git add <File>

Alles stagen
git add .

Nur Änderungen von schonmal getrackte Files stagen:
git add -u

Commit gestagete Änderungen
git commit <Filename>

Commit mit Message gestagete Änderungen:
git commit <filename> -m <message>

Commit alle gestageten Änderungen mit Message:
git commit -m <message>


Regex:
+ = match 1 or more
? = match 0 or 1 repetitions.
* = match 0 or MORE repetitions
. = Any character except a new line


R ead
E valuate
P rint
L oop


Debug dev_appserver:
Look for "ERROR" in logs (printout in terminal)
