from google.appengine.ext import ndb

class Message(ndb.Model):
    message_text = ndb.StringProperty()

class Person(ndb.Model):
    first_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    email = ndb.StringProperty()
