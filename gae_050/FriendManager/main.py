#!/usr/bin/env python
import os
import random

import jinja2
import webapp2

from models import Message, Person

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        return self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        return self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        return self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        result = self.render_template("index.html", params={})
        return result


class InfoHandler(BaseHandler):
    def get(self):
        result = self.render_template("infosite.html")
        return result


class ManagerHandler(BaseHandler):

    def get(self):
        persons = Person.query().fetch()
        result = self.render_template("manager.html",
                                      params={"persons_list": persons})
        return result

    def post(self):
        create_request = self.request.get("create_mock_entries", None)
        if create_request:
            create_contacts()

        persons = Person.query().fetch()
        result = self.render_template("manager.html",
                                      params={"persons_list": persons})
        return result


class BlogHandler(BaseHandler):
    def get(self):
        message_list = Message.query().fetch()
        return self.render_template("blog.html", params={"message_list": message_list})

    def post(self):
        new_message_text = self.request.get("text")
        new_message = Message(message_text=new_message_text)
        new_message.put()

        message_list = Message.query().fetch()
        return self.render_template("blog.html", params={"message_list": message_list})


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
    webapp2.Route("/info", InfoHandler),
    webapp2.Route("/manager", ManagerHandler),
    webapp2.Route("/blog", BlogHandler),
], debug=True)


def main():
    from paste import httpserver
    httpserver.serve(app, host='0.0.0.0', port='8080')


def create_contacts():
    for x in range(5):
        first_name = str(random.randint(1000000, 9000000))
        last_name = str(random.randint(1000000, 9000000))
        email = str(random.randint(1000000, 9000000)) + "@example.com"
        person = Person(first_name=first_name, last_name=last_name, email=email)
        person.put()

if __name__ == '__main__':
    main()
