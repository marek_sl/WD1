#!/usr/bin/env python
import os
import jinja2
import webapp2
import random

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        return self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        return self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        return self.response.out.write(template.render(params))



class MainHandler(BaseHandler):

    secret = str(random.randint(0, 10))
    guess = None
    guessed_numbers = []

    def get(self):
        return self.render_template("main.html", params={"did_guess": self.guess,
                                                          "guessed_numbers": self.guessed_numbers})

    def post(self):
        self.guess = self.request.get("some_text")
        self.guessed_numbers.append(self.guess)
        is_right = self.guess == self.secret
        return self.render_template("main.html", params={"input2": is_right,
                                                          "winnernumber": self.secret,
                                                          "did_guess": self.guess,
                                                          "guessed_numbers":self.guessed_numbers})


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
], debug=True)


def main():
    from paste import httpserver
    httpserver.serve(app, host='127.0.0.1', port='8080')


if __name__ == '__main__':
    main()
