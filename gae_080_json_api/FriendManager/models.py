from google.appengine.ext import ndb

# possible properties for the google datastore are documented on:
# https://cloud.google.com/appengine/docs/standard/python/ndb/entity-property-reference


class Message(ndb.Model):
    message_text = ndb.StringProperty()
    author = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    deleted = ndb.BooleanProperty(default=False)


class Person(ndb.Model):
    first_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    email = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    deleted = ndb.BooleanProperty(default=False)


class User(ndb.Model):
    nickname = ndb.StringProperty()
    password = ndb.StringProperty()
    email = ndb.StringProperty()
    website_link = ndb.StringProperty()
    role = ndb.StringProperty(default="member")
    created = ndb.DateTimeProperty(auto_now_add=True)
    deleted = ndb.BooleanProperty(default=False)