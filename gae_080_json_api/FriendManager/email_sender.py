from google.appengine.api import mail


def send_approved_mail(sender_address, receiver_address, receiver_nickname, website_name, website_url):
    # [START send_message]
    message = mail.EmailMessage(
        sender=sender_address,
        subject="Your account has been approved")

    message.to = "{}".format(receiver_address)
    message.body = """Dear {}:
Your {} project account has been approved.  You can now visit
{} and sign in using your Google Account to
access new features.
Please let us know if you have any questions.
""".format(receiver_nickname, website_name, website_url)
    message.send()
    # [END send_message]
