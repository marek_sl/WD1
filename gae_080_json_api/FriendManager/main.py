#!/usr/bin/env python
import os
import random

import jinja2
import webapp2
import json

from google.appengine.api import users
from google.appengine.api import app_identity
from google.appengine.api import urlfetch

from email_sender import send_approved_mail
from models import Message, Person, User

# WARNING: You can change this to anything, but Email authorization has to be set in
# https://console.cloud.google.com/appengine/settings
APPSPOT_EMAIL_SENDER = "email_chimp@{}.appspotmail.com".format(app_identity.get_application_id())
WEBSITE_URL = "https://{}.appspot.com".format(app_identity.get_application_id())
WEBSITE_NAME = "{}".format(app_identity.get_application_id())
template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


def get_login_user_infos(route="/"):
    user = users.get_current_user()
    url_redirect = "{}".format(route)
    if user:
        # test if user already in database
        # if not, add him to the database
        current_user = User.query(User.nickname == user.nickname(), User.email == user.email()).fetch()
        if not current_user:
            current_user = User(nickname=user.nickname(), email=user.email())
            current_user.put()
        else:
            current_user = current_user[0]
        logged_in = True
        logout_url = users.create_logout_url(url_redirect)
        login_url = None
        admin = User.query(User.role == "admin", User.email == user.email()).fetch()
        is_admin = bool(admin)

        # option with extra restricted site
        # if not admin:
        #    return self.redirect_to("denied")
    else:
        current_user=None
        logged_in = False
        logout_url = None
        is_admin = False
        login_url = users.create_login_url(url_redirect)
    return dict(logged_in=logged_in, login_url=login_url, logout_url=logout_url,is_admin=is_admin, user=current_user)

class BaseHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        return self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        return self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        return self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        params = get_login_user_infos("/")
        result = self.render_template("index.html", params=params)
        return result


class InfoHandler(BaseHandler):
    def get(self):
        params = get_login_user_infos("/info")
        result = self.render_template("infosite.html", params=params)
        return result


class AdminHandler(BaseHandler):
    def get(self):
        params = get_login_user_infos("/admin")
        users_list = User.query(User.deleted==False).fetch()
        params["users_list"]=users_list
        return self.render_template("admin.html", params=params)

    def post(self):
        pressed_button = self.request.get("button", None)
        if pressed_button == "delete":
            account_emails_to_delete = self.request.POST.getall('delete_mail')
            users = User.query(User.email.IN(account_emails_to_delete)).fetch()
            for user in users:
                # user.key.delete()
                user.deleted = True
                user.put()
        elif pressed_button == "send_mail":
            account_emails_to_delete = self.request.POST.getall('send_mail')
            users = User.query(User.email.IN(account_emails_to_delete)).fetch()
            for user in users:
                send_approved_mail(APPSPOT_EMAIL_SENDER, user.email, user.nickname, WEBSITE_NAME, WEBSITE_URL)
        elif pressed_button == "add":
            new_nickname = self.request.get("new_nickname")
            new_email = self.request.get("new_email")
            new_role = self.request.get("new_role")
            new_website_link = self.request.get("new_website_link")
            user = User(nickname=new_nickname, email=new_email, role=new_role, website_link=new_website_link)
            user.put()
        return self.redirect("admin")


class ManagerHandler(BaseHandler):
    def get(self):
        params = get_login_user_infos("manager")
        persons = Person.query(Person.deleted==False).fetch()
        params["persons_list"] = persons
        result = self.render_template("manager.html", params=params)
        return result

    def post(self):
        pressed_button = self.request.get("button", None)
        if pressed_button == "create_mock_entries":
            create_contacts()
        elif pressed_button == "delete":
            account_emails_to_delete = self.request.POST.getall('delete')
            persons = Person.query(Person.email.IN(account_emails_to_delete)).fetch()
            for person in persons:
                # person.key.delete()
                person.deleted = True
                person.put()
        elif pressed_button == "add":
            new_first_name = self.request.get("new_first_name")
            new_last_name = self.request.get("new_last_name")
            email = self.request.get("new_email")
            person = Person(first_name=new_first_name, last_name=new_last_name, email=email)
            person.put()

        return self.redirect("manager")


class BlogHandler(BaseHandler):
    def get(self):
        params = get_login_user_infos("blog")
        message_list = Message.query(Message.deleted == False).fetch()
        params["message_list"]=message_list
        return self.render_template("blog.html", params=params)

    def post(self):
        new_message_text = self.request.get("text")
        new_message_author = self.request.get("author")
        new_message = Message(message_text=new_message_text, author=new_message_author)
        new_message.put()

        return self.redirect_to("blog")


class EditMessageHandler(BaseHandler):
    def get(self, message_id):
        params = get_login_user_infos("blog")
        message = Message.get_by_id(int(message_id))
        params["message"]= message
        return self.render_template("message_edit.html", params=params)

    def post(self, message_id):
        new_text = self.request.get("some_text")
        author = self.request.get("author")
        message = Message.get_by_id(int(message_id))
        message.message_text = new_text
        message.author = author
        message.put()
        return self.redirect_to("blog")


class EditUserHandler(BaseHandler):
    def get(self, user_id):
        params = get_login_user_infos("/")

        # test if user is an admin
        user = users.get_current_user()
        if user:
            current_user = User.query(User.nickname == user.nickname(), User.email == user.email()).fetch()
            if not current_user:
                current_user = User(nickname=user.nickname(), email=user.email())
                current_user.put()
            admin = User.query(User.role == "admin", User.email == user.email()).fetch()
            if not admin:
                return self.redirect_to("denied")

        user = User.get_by_id(int(user_id))
        params["user"] = user
        return self.render_template("user_edit.html", params=params)

    def post(self, user_id):

        # test if user is an admin
        user = users.get_current_user()
        if user:
            current_user = User.query(User.nickname == user.nickname(), User.email == user.email()).fetch()
            if not current_user:
                current_user = User(nickname=user.nickname(), email=user.email())
                current_user.put()
            admin = User.query(User.role == "admin", User.email == user.email()).fetch()
            if not admin:
                return self.redirect_to("denied")

        nickname= self.request.get("nickname")
        email= self.request.get("email")
        role= self.request.get("role")
        website_link= self.request.get("website_link")
        user = User.get_by_id(int(user_id))
        user.nickname = nickname
        user.email = email
        user.role = role
        user.website_link = website_link
        user.put()
        return self.redirect_to("admin")


class DeleteMessageHandler(BaseHandler):
    def get(self, message_id):
        params = get_login_user_infos("/blog")

        message = Message.get_by_id(int(message_id))
        params["message"] = message
        return self.render_template("message_delete.html", params=params)

    def post(self, message_id):
        message = Message.get_by_id(int(message_id))
        # message.key.delete()
        message.deleted = True
        message.put()
        return self.redirect_to("blog")


class RestrictedHandler(BaseHandler):
    def get(self):
        params = get_login_user_infos("/")
        return self.render_template("restricted.html", params=params)


class WeatherHandler(BaseHandler):
    def get(self):
        params = get_login_user_infos("/weather")
        city = "munich"
        url = "http://samples.openweathermap.org/data/2.5/forecast?q=M%C3%BCnchen,DE&appid=b1b15e88fa797225412429c1c50c122a1"

        result = urlfetch.fetch(url)

        weather_info = json.loads(result.content)

        # params.update(weather_info) is possible, but we make a new key to have better data structure for jinja
        params["weather_info"] = weather_info
        params["city"] = city
        return self.render_template("weather.html", params=params)

app = webapp2.WSGIApplication([
    webapp2.Route("/", MainHandler),
    webapp2.Route("/info", InfoHandler),
    webapp2.Route("/weather", WeatherHandler, name="weather"),
    webapp2.Route("/admin", AdminHandler, name="admin"),
    webapp2.Route("/manager", ManagerHandler, name="manager"),
    webapp2.Route("/blog", BlogHandler, name="blog"),
    webapp2.Route("/denied", RestrictedHandler, name="denied"),
    webapp2.Route("/user/<user_id:\d+>/edit", EditUserHandler),
    webapp2.Route("/message/<message_id:\d+>/edit", EditMessageHandler),
    webapp2.Route("/message/<message_id:\d+>/delete", DeleteMessageHandler),
], debug=True)



def create_contacts():
    abc_ascii_codes = range(97, 123)
    ABC_ascii_codes = range(65, 91)
    possible_abc_ascii_codes = abc_ascii_codes + ABC_ascii_codes
    name_length = 10
    for x in range(5):
        first_name = "".join([chr(random.choice(possible_abc_ascii_codes)) for _ in range(name_length)])
        last_name = "".join([chr(random.choice(possible_abc_ascii_codes)) for _ in range(name_length)])
        email = "".join([chr(random.choice(possible_abc_ascii_codes)) for _ in range(name_length)]) + "@example.com"
        person = Person(first_name=first_name, last_name=last_name, email=email)
        person.put()


def main():
    from paste import httpserver
    httpserver.serve(app, host='0.0.0.0', port='8080')


if __name__ == '__main__':
    main()
