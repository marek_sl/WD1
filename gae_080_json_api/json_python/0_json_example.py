import json

if __name__ == '__main__':
    # Example 1: load from file
    with open("people.json", "r") as f:
        data = json.load(f)

    # Example 2: save to file
    with open("new_people.json", "w") as f:
        json.dump(data, f)

    # Example 3: dump to string
    data_string = json.dumps(data)

    print data_string
    print

    # Example 4: load from string
    new_data = json.loads(data_string)

    for entry in new_data:
        print entry
