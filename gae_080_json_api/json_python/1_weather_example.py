import json

if __name__ == '__main__':
    with open("weather.json", "r") as f:
        weather_info = json.load(f)

    print weather_info["list"][0]["main"]["humidity"]
    print weather_info["city"]["name"]
