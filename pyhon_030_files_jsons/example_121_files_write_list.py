planets = ["Merkur", "Venus", "Erde", "Mars", "Jupiter"]

filename = "planets.txt"

with open(filename,"w") as f:
    content = ",".join(planets) # join elements by adding a "," in-between
    f.write(content)

with open(filename,"r") as f:
    content = f.read()
    print content.split(",")  # seperate by "," and create list
