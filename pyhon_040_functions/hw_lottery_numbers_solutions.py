import random
# write a function which generates lottery numbers and returns a list


def lottery_generator():
    return random.sample(range(1, 50), 6)
