def square_number(x):
    """

    :param x: number to square
    :type x: int or float
    :return: the squared number
    :rtype: int or float
    """
    return x**2


def check_square_number():
    assert square_number(9) == 81
    assert square_number(-3) == 9


def cube_number(x):
    return x**3


def check_cube_number():
    assert cube_number(2)==8


# Assignment: Define a Test first, and then write the function which fulfills the test


if __name__ == '__main__':
    check_square_number()
    check_cube_number()
    print "Tests completed successfully"



